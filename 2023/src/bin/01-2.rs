use std::fs;

fn digits_in_line(mut line: &str) -> Vec<u32> {
    let mut digits = Vec::new();
    while !line.is_empty() {
        for (s, d) in [
            ("0", 0),
            ("1", 1),
            ("2", 2),
            ("3", 3),
            ("4", 4),
            ("5", 5),
            ("6", 6),
            ("7", 7),
            ("8", 8),
            ("9", 9),
            ("one", 1),
            ("two", 2),
            ("three", 3),
            ("four", 4),
            ("five", 5),
            ("six", 6),
            ("seven", 7),
            ("eight", 8),
            ("nine", 9),
        ] {
            if line.starts_with(s) {
                digits.push(d);
            }
        }
        line = &line[1..];
    }
    return digits;
}

fn calculate(s: &str) -> u32 {
    let mut sum: u32 = 0;
    for line in s.split("\n") {
        if line.is_empty() {
            continue;
        }
        let digits = digits_in_line(line);
        let value = digits.first().unwrap() * 10 + digits.last().unwrap();
        sum += value;
    }
    return sum;
}

fn main() {
    for filename in ["example/01-2.txt", "input/01-2.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
