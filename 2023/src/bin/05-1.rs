use std::fs;

fn calculate(file: &str) -> u64 {
    let mut lines = file.split("\n");
    let mut current_values: Vec<u64> = lines
        .next()
        .unwrap()
        .split_once(": ")
        .unwrap()
        .1
        .split(" ")
        .map(|s| s.parse().unwrap())
        .collect();
    let mut next_values: Vec<u64> = Vec::with_capacity(current_values.len());
    for line in lines {
        if line.is_empty() {
            continue;
        }
        if line.ends_with("map:") {
            current_values.append(&mut next_values);
            next_values = Vec::new();
        } else {
            if let [dest, source, length] = line
                .split(" ")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<u64>>()
                .as_slice()
            {
                let mut unmatched_current = Vec::with_capacity(current_values.len());
                for current in current_values {
                    if current >= *source && current < *source + *length {
                        next_values.push(*dest + (current - *source));
                    } else {
                        unmatched_current.push(current)
                    }
                }
                current_values = unmatched_current;
            } else {
                panic!("couldn't split line {}", line)
            }
        }
    }
    current_values.append(&mut next_values);
    *(current_values.iter().min().unwrap())
}

fn main() {
    for filename in ["example/05.txt", "input/05.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
