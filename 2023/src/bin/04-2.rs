use std::fs;

fn points_for_line(line: &str) -> u32 {
    let (_card_num, numbers) = line.split_once(":").unwrap();
    let (winners, entries) = numbers.split_once(" |").unwrap();
    let mut wins: u32 = 0;
    for have_i in (0..entries.len()).step_by(3) {
        for win_i in (0..winners.len()).step_by(3) {
            let have = &entries[have_i + 1..have_i + 3];
            let win = &winners[win_i + 1..win_i + 3];
            if have == win {
                wins += 1;
            }
        }
    }
    return wins;
}

fn calculate(file: &str) -> u32 {
    let mut lines: Vec<&str> = file.split("\n").collect();
    if lines.last().is_some_and(|s| s.is_empty()) {
        lines.pop();
    }
    let wins: Vec<u32> = lines.iter().map(|line| points_for_line(line)).collect();
    let mut cards: Vec<u32> = vec![1; wins.len()];
    let mut sum = 0;
    for i in 0..wins.len() {
        sum += cards[i];
        for j in (i + 1)..(i + wins[i] as usize + 1) {
            cards[j] += cards[i];
        }
    }
    sum
}

fn main() {
    for filename in ["example/04.txt", "input/04.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
