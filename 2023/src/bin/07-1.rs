use arrayvec::ArrayVec;
use std::cmp::Ordering;
use std::fs;

struct Hand {
    cards: Cards,
    score: HandScore,
    bid: u64,
}

#[derive(PartialEq, Eq, PartialOrd, Ord)]
enum HandScore {
    FiveOfAKind = 7,
    FourOfAKind = 6,
    FullHouse = 5,
    ThreeOfAKind = 4,
    TwoPair = 3,
    OnePair = 2,
    HighCard = 1,
}

type Cards = [Card; 5];

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
enum Card {
    C2 = 2,
    C3 = 3,
    C4 = 4,
    C5 = 5,
    C6 = 6,
    C7 = 7,
    C8 = 8,
    C9 = 9,
    CT = 10,
    CJ = 11,
    CQ = 12,
    CK = 13,
    CA = 14,
}

fn parse_card(c: char) -> Card {
    match c {
        '2' => Card::C2,
        '3' => Card::C3,
        '4' => Card::C4,
        '5' => Card::C5,
        '6' => Card::C6,
        '7' => Card::C7,
        '8' => Card::C8,
        '9' => Card::C9,
        'T' => Card::CT,
        'J' => Card::CJ,
        'Q' => Card::CQ,
        'K' => Card::CK,
        'A' => Card::CA,
        _ => panic!("unexpected card {:?}", c),
    }
}

fn parse_hand(line: &str) -> Hand {
    let (cards_string, bid) = line.split_once(" ").unwrap();
    let cards_vec: Vec<Card> = cards_string.chars().map(parse_card).collect::<Vec<Card>>();
    assert!(cards_vec.len() == 5);
    let cards: Cards = [
        cards_vec[0],
        cards_vec[1],
        cards_vec[2],
        cards_vec[3],
        cards_vec[4],
    ];
    Hand {
        cards,
        score: hand_score(cards),
        bid: bid.parse::<u64>().unwrap(),
    }
}

fn hand_score(cards: Cards) -> HandScore {
    let counts: ArrayVec<u8, 5> = hand_counts(cards);
    match counts[0] {
        5 => HandScore::FiveOfAKind,
        4 => HandScore::FourOfAKind,
        3 => match counts[1] {
            2 => HandScore::FullHouse,
            1 => HandScore::ThreeOfAKind,
            _ => panic!("unexpected counts[1] {:?}", counts),
        },
        2 => match counts[1] {
            2 => HandScore::TwoPair,
            1 => HandScore::OnePair,
            _ => panic!("unexpected counts[1] {:?}", counts),
        },
        1 => HandScore::HighCard,
        _ => panic!("unexpected counts[0] {:?}", counts),
    }
}

// result is sorted, largest count at start,
// all elements sum to 5
fn hand_counts(cards: Cards) -> ArrayVec<u8, 5> {
    let mut counts_map: Vec<(Card, u8)> = Vec::with_capacity(5);
    for card in cards {
        let mut found: Option<(usize, u8)> = None;
        for (i, (prev_card, count)) in counts_map.iter().enumerate() {
            if *prev_card == card {
                found = Some((i, *count));
                break;
            }
        }
        match found {
            Some((i, count)) => counts_map[i] = (card, count + 1),
            None => counts_map.push((card, 1)),
        }
    }

    let mut counts: ArrayVec<u8, 5> = ArrayVec::new();
    for (_, count) in counts_map {
        counts.push(count);
    }
    assert!(counts.iter().sum::<u8>() == 5);
    counts.sort_unstable_by(|a, b| b.cmp(a));
    counts
}

fn hand_cmp(hand1: &Hand, hand2: &Hand) -> Ordering {
    match HandScore::cmp(&hand1.score, &hand2.score) {
        Ordering::Less => Ordering::Less,
        Ordering::Greater => Ordering::Greater,
        Ordering::Equal => <[Card; 5]>::cmp(&hand1.cards, &hand2.cards),
    }
}

fn calculate(file: &str) -> u64 {
    let mut lines: Vec<&str> = file.split("\n").collect();
    if lines.last().is_some_and(|s| s.is_empty()) {
        lines.pop();
    }
    let mut hands: Vec<Hand> = lines.iter().map(|line| parse_hand(line)).collect();
    hands.sort_unstable_by(hand_cmp);
    hands
        .iter()
        .enumerate()
        .map(|(i, hand)| hand.bid * (i as u64 + 1))
        .sum()
}

fn main() {
    for filename in ["example/07.txt", "input/07.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
