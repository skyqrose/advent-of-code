use std::fs;

fn points_for_line(line: &str) -> u32 {
    let (_card_num, numbers) = line.split_once(":").unwrap();
    let (winners, entries) = numbers.split_once(" |").unwrap();
    let mut wins: u32 = 0;
    for have_i in (0..entries.len()).step_by(3) {
        for win_i in (0..winners.len()).step_by(3) {
            let have = &entries[have_i + 1..have_i + 3];
            let win = &winners[win_i + 1..win_i + 3];
            if have == win {
                wins += 1;
            }
        }
    }
    if wins == 0 {
        0
    } else {
        1 << (wins - 1)
    }
}

fn calculate(file: &str) -> u32 {
    let mut lines: Vec<&str> = file.split("\n").collect();
    if lines.last().is_some_and(|s| s.is_empty()) {
        lines.pop();
    }
    return lines.iter().map(|line| points_for_line(line)).sum();
}

fn main() {
    for filename in ["example/04.txt", "input/04.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
