use std::fs;

fn calculate(file: &str) -> String {
    let mut sum: u32 = 0;
    for line in file.split("\n") {
        if line.is_empty() {
            continue;
        }
        let (game_str, hands) = line.split_once(": ").unwrap();
        let mut valid: bool = true;
        for hand in hands.split("; ") {
            for color_and_count in hand.split(", ") {
                let (count_str, color) = color_and_count.split_once(" ").unwrap();
                let count: u16 = count_str.parse().unwrap();
                if color == "red" && count > 12 {
                    valid = false;
                }
                if color == "green" && count > 13 {
                    valid = false;
                }
                if color == "blue" && count > 14 {
                    valid = false;
                }
                if valid == false {
                    break;
                }
            }
            if valid == false {
                break;
            }
        }
        if valid == false {
            continue;
        }
        sum += game_str[5..].parse::<u32>().unwrap();
    }
    return sum.to_string();
}

fn main() {
    for filename in ["example/02.txt", "input/02.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
