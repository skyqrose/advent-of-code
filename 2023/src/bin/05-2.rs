use std::fs;

fn pair_up(xs: &Vec<u64>) -> Vec<(u64, u64)> {
    let mut result: Vec<(u64, u64)> = Vec::with_capacity(xs.len() / 2);
    assert!(xs.len() % 2 == 0, "not even length");
    for i in (0..xs.len()).step_by(2) {
        result.push((xs[i], xs[i + 1]));
    }
    result
}

fn parse_seeds(line: &str) -> Vec<(u64, u64)> {
    let ints: Vec<u64> = line
        .split_once(": ")
        .unwrap()
        .1
        .split(" ")
        .map(|s| s.parse().unwrap())
        .collect();
    pair_up(&ints)
        .into_iter()
        .map(|(seed, len)| (seed, seed + len))
        .collect()
}

fn calculate(file: &str) -> u64 {
    let mut lines = file.split("\n");
    let mut current_values: Vec<(u64, u64)> = parse_seeds(lines.next().unwrap());
    println!("seeds {:?}", current_values);

    let mut next_values: Vec<(u64, u64)> = Vec::with_capacity(current_values.len());
    for line in lines {
        if line.is_empty() {
            continue;
        }
        if line.ends_with("map:") {
            println!(
                "{} new:{:?} left_over:{:?}",
                line, next_values, current_values
            );
            current_values.append(&mut next_values);
            next_values = Vec::new();
        } else {
            if let [dest_, source_, maplen_] = line
                .split(" ")
                .map(|s| s.parse::<u64>().unwrap())
                .collect::<Vec<u64>>()
                .as_slice()
            {
                let dest = *dest_;
                let source = *source_;
                let maplen = *maplen_;
                let sourceend = source + maplen;
                let mut unmatched_current = Vec::with_capacity(current_values.len());
                for (currstart, currend) in current_values {
                    if currstart < source && currend > source && currend <= sourceend {
                        next_values.push((source + dest - source, currend + dest - source));
                        unmatched_current.push((currstart, source));
                    } else if currstart < source && currend > sourceend {
                        next_values.push((source + dest - source, sourceend + dest - source));
                        unmatched_current.push((currstart, source));
                        unmatched_current.push((sourceend, currend));
                    } else if currstart >= source && currend <= sourceend {
                        next_values.push((currstart + dest - source, currend + dest - source));
                    } else if currstart >= source && currstart < sourceend && currend > sourceend {
                        next_values.push((currstart + dest - source, sourceend + dest - source));
                        unmatched_current.push((sourceend, currend));
                    } else {
                        unmatched_current.push((currstart, currend))
                    }
                }
                current_values = unmatched_current;
            } else {
                panic!("couldn't split line {}", line)
            }
        }
    }
    current_values.append(&mut next_values);
    current_values
        .into_iter()
        .map(|(start, _end)| start)
        .min()
        .unwrap()
}

fn main() {
    for filename in ["example/05.txt", "input/05.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
