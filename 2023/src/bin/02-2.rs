use std::cmp;
use std::fs;

struct Bag {
    red: u32,
    green: u32,
    blue: u32,
}

fn calculate(file: &str) -> String {
    let mut sum: u32 = 0;
    for line in file.split("\n") {
        if line.is_empty() {
            continue;
        }
        let mut bag = Bag {
            red: 0,
            green: 0,
            blue: 0,
        };
        let (_game_str, hands) = line.split_once(": ").unwrap();
        for hand in hands.split("; ") {
            for color_and_count in hand.split(", ") {
                let (count_str, color) = color_and_count.split_once(" ").unwrap();
                let count: u32 = count_str.parse().unwrap();
                if color == "red" {
                    bag.red = cmp::max(bag.red, count);
                }
                if color == "green" {
                    bag.green = cmp::max(bag.green, count);
                }
                if color == "blue" {
                    bag.blue = cmp::max(bag.blue, count);
                }
            }
        }
        let power = bag.red * bag.green * bag.blue;
        sum += power;
    }
    return sum.to_string();
}

fn main() {
    for filename in ["example/02.txt", "input/02.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
