use regex;
use std::fs;

fn calculate(file: &str) -> String {
    let number_re = regex::Regex::new(r"\d+").unwrap();
    let symbol_re = regex::Regex::new(r"[^.\d]").unwrap();
    let lines: Vec<&str> = file.split("\n").collect();
    let mut sum: u32 = 0;
    for (i, line) in lines.iter().enumerate() {
        let above = if i > 0 { lines.get(i - 1) } else { None };
        let below = lines.get(i + 1).filter(|line| !line.is_empty());
        for match_ in number_re.find_iter(line) {
            let start = match_.start();
            let end = match_.end();
            let beforestart = if start > 0 { start - 1 } else { start };
            let afterend = if end < line.len() { end + 1 } else { end };
            if above.is_some_and(|above| symbol_re.is_match(&above[beforestart..afterend]))
                || below.is_some_and(|below| symbol_re.is_match(&below[beforestart..afterend]))
                || (start > 0 && symbol_re.is_match(&line[start - 1..start]))
                || (end < line.len() && symbol_re.is_match(&line[end..end + 1]))
            {
                let digits = match_.as_str();
                sum += digits.parse::<u32>().unwrap();
            }
        }
    }
    return sum.to_string();
}

fn main() {
    for filename in ["example/03-1.txt", "input/03.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
