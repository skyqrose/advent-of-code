use std::fs;
fn calculate(s: String) -> u32 {
    let mut sum: u32 = 0;
    for line in s.split("\n") {
        if line.is_empty() {
            continue;
        }
        let mut digits = Vec::new();
        for char in line.chars() {
            let codepoint: u32 = char as u32;
            if codepoint >= 48 && codepoint < 58 {
                digits.push(codepoint - 48);
            }
        }
        let value = digits.first().unwrap() * 10 + digits.last().unwrap();
        sum += value;
    }
    return sum;
}

fn main() {
    for filename in ["example/01-1.txt", "input/01-1.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(file);
        println!("{:?}", result);
    }
}
