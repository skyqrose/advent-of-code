use regex;
use std::fs;

struct Num {
    num: u32,
    line: usize,
    start: usize,
    end: usize,
}

fn calculate(file: &str) -> u32 {
    let number_re = regex::Regex::new(r"\d+").unwrap();
    let mut nums: Vec<Num> = Vec::new();
    let lines: Vec<&str> = file.split("\n").collect();
    /*
    if lines.last().is_some_and(String::is_empty) {
        lines.drop_last();
    }*/
    for (i, line) in lines.iter().enumerate() {
        for match_ in number_re.find_iter(line) {
            nums.push(Num {
                num: match_.as_str().parse::<u32>().unwrap(),
                line: i,
                start: match_.start(),
                end: match_.end(),
            })
        }
    }

    let mut sum: u32 = 0;
    for (line_i, line) in lines.iter().enumerate() {
        for (gear_i, _) in line.match_indices("*") {
            let above = line_i.checked_sub(1).unwrap_or(0);
            let below = line_i + 1;
            let matching_nums: Vec<u32> = nums
                .iter()
                .filter_map(|num| {
                    if num.line >= above
                        && num.line <= below
                        && num.start <= gear_i + 1
                        && num.end > gear_i - 1
                    {
                        Some(num.num)
                    } else {
                        None
                    }
                })
                .collect();
            match matching_nums[..] {
                [num1, num2] => {
                    sum += num1 * num2;
                }
                _ => (),
            }
        }
    }
    return sum;
}

fn main() {
    for filename in ["example/03-1.txt", "input/03.txt"] {
        println!("{}", filename);
        let file: String = fs::read_to_string(filename).unwrap();
        let result = calculate(&file);
        println!("{:?}", result);
    }
}
